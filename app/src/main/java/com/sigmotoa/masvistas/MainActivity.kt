package com.sigmotoa.masvistas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btn1: Button = findViewById(R.id.btn1)
        btn1.setOnClickListener{ Toast.makeText( this, "You select the button1", Toast.LENGTH_SHORT).show()}
        val btn2: Button = findViewById(R.id.btn2)
        btn2.setOnClickListener{Toast.makeText( this, "You select the button2", Toast.LENGTH_SHORT).show()}
        val btn3: Button = findViewById(R.id.btn3)
        btn3.setOnClickListener{Toast.makeText( this, "You select the button3", Toast.LENGTH_SHORT).show()}
        val btn4: Button = findViewById(R.id.btn4)
        btn4.setOnClickListener{Toast.makeText( this, "You select the button4", Toast.LENGTH_SHORT).show()}
        val btn5: Button = findViewById(R.id.btn5)
        btn5.setOnClickListener{Toast.makeText( this, "You select the button5", Toast.LENGTH_SHORT).show()}
        val btn6: Button = findViewById(R.id.btn6)
        btn6.setOnClickListener{Toast.makeText( this, "You select the button6", Toast.LENGTH_SHORT).show()}
    }
}